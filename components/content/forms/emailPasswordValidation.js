import * as Yup from 'yup';

const uppercaseRegex = new RegExp('^(?=.*[A-Z])');
const lowercaseRegex = new RegExp('^(?=.*[a-z])');
const numberRegex = new RegExp('^(?=.*[0-9])');
const specialCharRegex = new RegExp('^(?=.*[!@#$%^&*])');
const minCharRegex = new RegExp('^(?=.{8,})');

export const validationSchema = Yup.object({
  email: Yup.string().email('Invalid email format').required('Required'),
  password: Yup.string()
    // .min(8)
    .matches(minCharRegex, 'Password must be at least 8 characters')
    .matches(uppercaseRegex, 'Must include at least One Uppercase Letter')
    .matches(lowercaseRegex, 'Must include at least One Lowercase Letter')
    .matches(numberRegex, 'Must include at least One Number')
    .matches(specialCharRegex, 'Must include at least One Special Character')
    .required('Please Enter your password'),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref('password'), ''], 'Passwords must match')
    .required('Required')
});
