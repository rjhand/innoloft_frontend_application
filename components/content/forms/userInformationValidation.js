import * as Yup from 'yup';

const numberRegex = new RegExp('^(?=.*[0-9])');

export const validationSchema = Yup.object().shape({
  firstName: Yup.string()
    .transform((value, originalValue) => originalValue.trim())
    .min(2, 'First Name must be at least 2 characters')
    .required('Required'),
  lastName: Yup.string()
    .transform((value, originalValue) => originalValue.trim())
    .min(2, 'Last Name must be at least 2 characters')
    .required('Required'),
  country: Yup.string().required('Required'),
  address: Yup.object().shape({
    street: Yup.string()
      .transform((value, originalValue) => originalValue.trim())
      .min(3, 'Street must be at least 3 characters')
      .matches(
        /^(?=.*[A-Z])|(?=.*[a-z])|(?=.*[0-9])/,
        'Street can only be alphanumeric characters'
      )
      .required('Required'),
    houseNumber: Yup.string()
      .transform((value, originalValue) => originalValue.trim())
      .matches(numberRegex, 'House Number must be a number')
      .required('Required'),
    postalCode: Yup.string()
      .required('Required')
      .transform((value, originalValue) => originalValue.trim())
      .matches(numberRegex, 'Postal Code must be a number')
      .min(4, 'Postal Code must be at least 4 numbers')
      .max(5, 'Postal Code can be a maximum of 5 numbers')
  })
});
