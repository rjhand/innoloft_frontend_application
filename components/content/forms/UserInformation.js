import React, { useState } from 'react';
import { Formik, Form } from 'formik';
import axios from 'axios';

import { validationSchema } from './userInformationValidation';
import Inputs from './Inputs';

import './UserInformation.css';
import SaveIcon from '@material-ui/icons/Save';
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Button
} from '@material-ui/core';

const initialValues = {
  firstName: '',
  lastName: '',
  address: {
    street: '',
    houseNumber: '',
    postalCode: ''
  },
  country: ''
};

const UserInformation = () => {
  const [formValues, setFormValues] = useState(null);
  const [country, setCountry] = useState('');

  const handleChange = (event) => {
    setCountry(event.target.value);
  };

  const onSubmit = (values, onSubmitProps) => {
    console.log('form data:>>', values);
    console.log('submit props', onSubmitProps);
    axios
      .post('random/url/')
      .then((response) => {
        console.log(response);
        alert('UPDATED!');
        onSubmitProps.setSubmitting(false);
        onSubmitProps.resetForm();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div className='userInformation'>
      <Formik
        initialValues={formValues || initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
        //good for few fields
        validateOnMount
        enableReinitialize
      >
        {(formik) => {
          return (
            <Form className='userInformation__form'>
              <div className='userInformation__input'>
                <Inputs
                  id='firstName'
                  control='input'
                  type='text'
                  label='First Name'
                  name='firstName'
                  formik={formik}
                />
              </div>

              <div className='userInformation__input'>
                <Inputs
                  id='lastName'
                  control='input'
                  type='text'
                  label='Last Name'
                  name='lastName'
                  formik={formik}
                />
              </div>

              <div className='userInformation__input'>
                <Inputs
                  id='street'
                  control='input'
                  type='text'
                  label='Street'
                  name='address.street'
                  formik={formik}
                />
              </div>

              <div className='userInformation__input'>
                <Inputs
                  id='houseNumber'
                  control='input'
                  type='text'
                  label='House Number'
                  name='address.houseNumber'
                  formik={formik}
                />
              </div>

              <div className='userInformation__input'>
                <Inputs
                  id='postalCode'
                  control='input'
                  type='text'
                  label='Postal Code'
                  name='address.postalCode'
                  formik={formik}
                />
              </div>

              <div className='userInformation__input'>
                <FormControl
                  name={country}
                  fullWidth='true'
                  // variant='none'
                  margin='dense'
                >
                  <InputLabel>Country</InputLabel>
                  <Select
                    labelId={formik.values.country}
                    value={formik.values.country}
                    onChange={formik.handleChange('country')}
                    name={country}
                  >
                    <MenuItem name={country} value={'germany'}>
                      Germany
                    </MenuItem>
                    <MenuItem name={country} value={'austria'}>
                      Austria
                    </MenuItem>
                    <MenuItem name={country} value={'switzerland'}>
                      Switzerland
                    </MenuItem>
                  </Select>
                </FormControl>
              </div>

              <div className='userInformation__button'>
                <Button
                  type='submit'
                  disabled={!formik.isValid || formik.isSubmitting}
                  variant='outlined'
                  endIcon={<SaveIcon />}
                  color='primary'
                  fullWidth='true'
                >
                  Update
                </Button>
              </div>
            </Form>
          );
        }}
      </Formik>
      <div className='userInformation__cancel'>
        <Button
          type='submit'
          variant='outlined'
          color='secondary'
          fullWidth='true'
          // Example (pseudo-code version): onClick={() => history.push()}
        >
          Cancel
        </Button>
      </div>
    </div>
  );
};

export default UserInformation;
