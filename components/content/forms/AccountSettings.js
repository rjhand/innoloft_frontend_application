import React, { useState } from 'react';
import { Formik, Form } from 'formik';
import axios from 'axios';

import { validationSchema } from './emailPasswordValidation';

import Inputs from './Inputs';

import './AccountSettings.css';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';

const initialValues = {
  email: '',
  password: '',
  confirmPassword: ''
};

const AccountSettings = () => {
  const [formValues, setFormValues] = useState(null);

  const onSubmit = (values, onSubmitProps) => {
    // console.log('form data:>>', values);
    // console.log('submit props', onSubmitProps);

    axios
      .post('random/url/')
      .then((response) => {
        console.log(response);
        alert('UPDATED!');
        onSubmitProps.setSubmitting(false);
        onSubmitProps.resetForm();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div className='accountSettings'>
      <Formik
        initialValues={formValues || initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
        //good for few fields
        validateOnMount
        enableReinitialize
      >
        {(formik) => {
          return (
            <Form className='accountSettings__form'>
              <div className='accountSettings__input'>
                <Inputs
                  control='input'
                  type='email'
                  label='Email'
                  name='email'
                  formik={formik}
                />
              </div>

              <div className='accountSettings__input'>
                <Inputs
                  control='input'
                  type='password'
                  label='Password'
                  name='password'
                  formik={formik}
                />
              </div>

              <div className='accountSettings__input'>
                <Inputs
                  control='input'
                  type='password'
                  label='Confirm Password'
                  name='confirmPassword'
                  formik={formik}
                />
              </div>

              <div className='accountSettings__button'>
                <Button
                  type='submit'
                  disabled={!formik.isValid || formik.isSubmitting}
                  variant='outlined'
                  endIcon={<SaveIcon />}
                  color='primary'
                  fullWidth='true'
                >
                  Update
                </Button>
              </div>
            </Form>
          );
        }}
      </Formik>
      <div className='accountSettings__cancel'>
        <Button
          type='submit'
          variant='outlined'
          color='secondary'
          fullWidth='true'
          // Example (pseudo-code version): onClick={() => history.push()}
        >
          Cancel
        </Button>
      </div>
    </div>
  );
};

export default AccountSettings;
