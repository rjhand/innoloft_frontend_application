import React from 'react';

import TextField from '@material-ui/core/TextField';
import { getIn } from 'formik';

const Input = (props) => {
  const { label, type, name, formik } = props;
  const { values, handleBlur, handleChange, errors, touched } = formik;

  return (
    <TextField
      error={getIn(touched, `${name}`) && getIn(errors, `${name}`)}
      label={label}
      name={name}
      type={type}
      value={values[name]}
      onChange={handleChange}
      onBlur={handleBlur}
      helperText={
        getIn(touched, `${name}`) && getIn(errors, `${name}`)
        // getIn(errors, `${name}`)
      }
      margin='dense'
      variant='outlined'
    />
  );
};

export default Input;
