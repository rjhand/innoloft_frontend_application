import React from 'react';
import { drawerWidth } from '../Main';
import SidebarContent from './SidebarContent';
import { Drawer, Hidden } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth
  }
}));

const SideBar = ({ window, handleDrawerToggle, mobileOpen }) => {
  const classes = useStyles();
  const theme = useTheme();

  const container =
    window !== undefined ? () => window().document.body : undefined;

  const drawer = ( // ALL CONTENT THAT IS IN SIDEBAR
    <div>
      <div className={classes.toolbar} />
      <SidebarContent />
    </div>
  );

  return (
    <>
      <Hidden smUp implementation='css'>
        <Drawer
          container={container}
          variant='temporary'
          anchor={theme.direction === 'rtl' ? 'right' : 'left'}
          open={mobileOpen}
          onClose={handleDrawerToggle}
          classes={{
            paper: classes.drawerPaper
          }}
          ModalProps={{
            keepMounted: true // Better open performance on mobile.
          }}
        >
          {drawer}
        </Drawer>
      </Hidden>
      {/* WHETHER SIDEBAR MENU APPEARS IN DESKTOP SCREEN  */}
      <Hidden xsDown implementation='css'>
        <Drawer
          classes={{
            paper: classes.drawerPaper
          }}
          variant='permanent'
          open
        >
          {drawer}
        </Drawer>
      </Hidden>
    </>
  );
};

export default SideBar;
