import React from 'react';
import './SideBarOption.css';

const SideBarOption = (props) => {
  const { text, Icon } = props;

  return (
    <div className='sideBarOption'>
      <Icon />
      <h2>{text}</h2>
    </div>
  );
};

export default SideBarOption;
