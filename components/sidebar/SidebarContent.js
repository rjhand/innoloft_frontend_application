import React from 'react';
import SideBarOption from './SideBarOption';
import './SidebarContent.css';
import {
  HomeOutlined,
  BusinessOutlined,
  SettingsOutlined,
  AccountBox,
  AssessmentOutlined,
  RssFeed
} from '@material-ui/icons';

const SidebarContent = () => {
  return (
    <div className='sidebarContent'>
      <SideBarOption Icon={HomeOutlined} text={'Home'} />
      <SideBarOption Icon={AccountBox} text={'My Account'} />
      <SideBarOption Icon={BusinessOutlined} text={'My Company'} />
      <SideBarOption Icon={SettingsOutlined} text={'My Settings'} />
      <SideBarOption Icon={RssFeed} text={'News'} />
      <SideBarOption Icon={AssessmentOutlined} text={'Analytics'} />
    </div>
  );
};

export default SidebarContent;
