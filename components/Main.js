import React, { useState } from 'react';

import SideBar from './sidebar/SideBar';
import Header from './navbar/Header';
import Content from './content/Content';

import { CssBaseline } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

// DRAWER WIDTH
export const drawerWidth = 210;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex'
  },
  toolbar: theme.mixins.toolbar,
  // EFFECTS WHERE MAIN CONTENT IS ON PAGE IN DESKTOP
  // using these for breakpoints instead of SCSS, for example I would start by:
  //   @media only screen and (max-width: 540px) {
  //    .sidebar {
  //     display: none;
  //    }
  //   }
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0
    }
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3)
  }
}));

const Main = () => {
  const [mobileOpen, setMobileOpen] = useState(false);
  const classes = useStyles();

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      {/* HEADER */}
      <Header
        drawerWidth={drawerWidth}
        handleDrawerToggle={handleDrawerToggle}
      />

      <nav className={classes.drawer} aria-label='folders'>
        {/* WHETHER DRAWER SHOWS UP WHEN HAMBURGER ICON CLICKED MOBILE MENU */}
        <SideBar
          handleDrawerToggle={handleDrawerToggle}
          mobileOpen={mobileOpen}
        />
      </nav>

      {/* ALL CONTENT IN MAIN */}
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <Content />
      </main>
    </div>
  );
};

export default Main;
