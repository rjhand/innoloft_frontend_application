import React, { useState } from 'react';

import './HeaderOption.css';
import { makeStyles } from '@material-ui/core/styles';
import { IconButton, Badge, MenuItem, Menu } from '@material-ui/core';
import { Mail, Notifications, Language } from '@material-ui/icons';
import MoreIcon from '@material-ui/icons/MoreVert';

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'flex'
    }
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('sm')]: {
      display: 'none'
    }
  }
}));

const HeaderOption = () => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = useState(null);

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const menuId = 'primary-search-account-menu';

  // LANGUAGE SELECTOR MENU
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <div className='headerOption'>
        <MenuItem onClick={handleMenuClose}>English</MenuItem>
      </div>
      <div className='headerOption'>
        <MenuItem onClick={handleMenuClose}>Deutsch</MenuItem>
      </div>
      <div className='headerOption'>
        <MenuItem onClick={handleMenuClose}>中文</MenuItem>
      </div>
    </Menu>
  );

  const mobileMenuId = 'primary-search-account-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <div className='headerOption'>
        <MenuItem>
          <IconButton aria-label='show 4 new mails' color='inherit'>
            <Badge badgeContent={4} color='secondary'>
              <Mail />
            </Badge>
          </IconButton>
          <p>Messages</p>
        </MenuItem>
      </div>

      <div className='headerOption'>
        <MenuItem>
          <IconButton aria-label='show 11 new notifications' color='inherit'>
            <Badge badgeContent={11} color='secondary'>
              <Notifications />
            </Badge>
          </IconButton>
          <p>Notifications</p>
        </MenuItem>
      </div>

      <div className='headerOption'>
        <MenuItem onClick={handleProfileMenuOpen}>
          <IconButton
            aria-label='account of current user'
            aria-controls='primary-search-account-menu'
            aria-haspopup='true'
            color='inherit'
          >
            <Language />
          </IconButton>
          <p>EN</p>
        </MenuItem>
      </div>
    </Menu>
  );

  return (
    <div className={classes.grow}>
      <div className={classes.grow} />
      <div className={classes.sectionDesktop}>
        <div className='headerOption'>
          <IconButton aria-label='show 4 new mails' color='inherit'>
            <Badge badgeContent={4} color='secondary'>
              <Mail />
            </Badge>
          </IconButton>
        </div>

        <div className='headerOption'>
          <IconButton aria-label='show 17 new notifications' color='inherit'>
            <Badge badgeContent={17} color='secondary'>
              <Notifications />
            </Badge>
          </IconButton>
        </div>

        <div className='headerOption'>
          <IconButton
            edge='end'
            aria-label='account of current user'
            aria-controls={menuId}
            aria-haspopup='true'
            onClick={handleProfileMenuOpen}
            color='inherit'
          >
            <Language />
          </IconButton>
        </div>
      </div>

      <div className={classes.sectionMobile}>
        <div className='headerOption'>
          <IconButton
            aria-label='show more'
            aria-controls={mobileMenuId}
            aria-haspopup='true'
            onClick={handleMobileMenuOpen}
            color='inherit'
          >
            <MoreIcon />
          </IconButton>
        </div>
      </div>
      {renderMobileMenu}
      {renderMenu}
    </div>
  );
};

export default HeaderOption;
