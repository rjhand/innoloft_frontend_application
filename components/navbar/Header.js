import React from 'react';

import { drawerWidth } from '../Main';
import HeaderOption from './HeaderOption';

import { AppBar, IconButton, Toolbar } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 100
  },

  // EFFECTS APPBAR CONTENT DESKTOP
  appBar: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth
    }
  },

  // CONTROLS WHEN HAMBURGER ICON APPEARS (MOBILE)
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none'
    }
  }
}));

const Header = ({ handleDrawerToggle }) => {
  const classes = useStyles();

  return (
    <AppBar position='fixed' color='white' className={classes.appBar}>
      <Toolbar>
        {/* HAMBURGER ICON */}
        <IconButton
          color='inherit'
          aria-label='open drawer'
          edge='start'
          onClick={handleDrawerToggle}
          className={classes.menuButton}
        >
          <MenuIcon />
        </IconButton>

        {/* PUT LOGO HERE */}
        <a href={'https://innoloft.com/public/'}>
          <img
            className='Header__innerImageLogo'
            src={'https://img.innoloft.de/logo_innoloft.svg'}
            alt=''
          />
        </a>
        <div className={classes.grow} />
        <HeaderOption />
      </Toolbar>
    </AppBar>
  );
};

export default Header;
